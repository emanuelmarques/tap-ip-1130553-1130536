package isep.tap.ip.datareaders

import scala.xml.XML
import isep.tap.ip.model._

object XMLReader {

  def getProductionDataFromFile(filename: String): Production = {

    val opt = XMLReader(filename)
    opt match {
      case None         => Production.apply(List[Physical](), List[Task](), List[Human](), List[Product](), List[Order]())
      case Some(parser) => parser(filename)
    }
  }

  def apply(filename: String): Option[(String) => Production] =
    filename match {
      case f if f.endsWith(".xml") => Some(parseXML)
      case f                       => None
    }

  def parseXML(filename: String): Production = {

    val filePath = System.getProperty("user.dir") + "/files/" + filename
    val xmlFile = XML.loadFile(filePath)

    // Retrieve Physical Resources
    val physicalResourcesXML = xmlFile \ "PhysicalResources" \ "Physical"
    val physicalResources = physicalResourcesXML.map(node => Physical(node.attributes("id").toString(), node.attributes("type").toString())).toList

    // Retrieve Tasks
    val tasksXML = xmlFile \ "Tasks" \ "Task"
    val mappedTasks = tasksXML.map(node =>
      Task(node.attributes("id").toString(),
        node.attributes("time").toString().toInt, (node \ "PhysicalResource").map(childNode => PhysicalResource(childNode.attributes("prstype").toString())).toList)).toList

    // Retrieve Human Resources
    val humansXML = xmlFile \ "HumanResources" \ "Human"
    val mappedHR = humansXML.map(node => Human(node.attributes("id").toString(), node.attributes("name").toString(), (node \ "Handles").map(childNode => Handles(childNode.attributes("type").toString())).toList)).toList

    // Retrieve Products
    val productsXML = xmlFile \ "Products" \ "Product" //> products  : scala.xml.NodeSeq = NodeSeq(<Product name="Product 1" id="PRD_1"
    val mappedProducts = productsXML.map(node => Product(node.attributes("id").toString(), node.attributes("name").toString(), (node \ "Process").map(childNode => Process(childNode.attributes("tskref").toString())).toList)).toList

    // Retrieve Orders
    val ordersXML = xmlFile \ "Orders" \ "Order"
    val mappedOrders = ordersXML.map(node => Order(node.attributes("id").toString(), node.attributes("prdref").toString(), node.attributes("quantity").toString().toInt)).toList

    Production(physicalResources, mappedTasks, mappedHR, mappedProducts, mappedOrders)
  }

}