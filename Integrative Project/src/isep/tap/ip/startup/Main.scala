package isep.tap.ip.startup

import javafx.application.Application
import scala.xml.XML
import isep.tap.ip.model._
import isep.tap.ip.datareaders.XMLReader
import isep.tap.ip.datawriters.XMLWriter
import isep.tap.ip.services._

object Main {

  def main(args: Array[String]): Unit = {
    println("Please insert the name of the file to read: ")

    val inputFile = scala.io.StdIn.readLine()

    println("Please insert the name you want for the output file: ")
    val outputFileName = scala.io.StdIn.readLine()

    //val scheduler = new ProductionParallelSchedulingService()

    val production = XMLReader.getProductionDataFromFile(inputFile)

    val scheduler = ProductionFactory.newProductionFactory()
    val schedule = scheduler.getProductionSchedule(production, 3)

    val outputPath = System.getProperty("user.dir") + "/files/" + outputFileName
    XMLWriter.writeToXml(outputPath, schedule)
  }
}