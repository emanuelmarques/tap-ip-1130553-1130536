package isep.tap.ip.model

case class Product(
  id: String,
  name: String,
  processes: List[Process])