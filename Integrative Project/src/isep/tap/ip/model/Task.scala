package isep.tap.ip.model

case class Task(
  id: String,
  time: Int,
  resources: List[PhysicalResource])