package isep.tap.ip.model.v2

import isep.tap.ip.model._
case class OrderTask(
  task: Task,
  taskSequence: Int,
  order: Order,
  productNumber: Int)