package isep.tap.ip.model

case class Order(
  id: String,
  prdref: String,
  quantity: Int)