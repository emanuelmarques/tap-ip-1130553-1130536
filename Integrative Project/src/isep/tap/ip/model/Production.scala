package isep.tap.ip.model

case class Production(
  PhysicalResources: List[Physical],
  Tasks: List[Task],
  HumanResources: List[Human],
  Products: List[Product],
  Orders: List[Order])