package isep.tap.ip.model

case class TaskSchedule(
  order: String,
  productNumber: Int,
  task: String,
  start: Int,
  end: Int,
  physicalResources: List[Physical],
  humanResources: List[Human])