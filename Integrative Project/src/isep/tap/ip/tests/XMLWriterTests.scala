package isep.tap.ip.tests

import isep.tap.ip.model._
import org.scalatest.FunSuite
import scala.xml.XML
import isep.tap.ip.datawriters.XMLWriter
import java.nio.file.Files
import java.nio.file.Paths
import java.io.File

class XMLWriterTests extends FunSuite {

  test("writeToXml_ValidInput_FileCreated") {

    // Arrange
    val schedule = Schedule(
      List[TaskSchedule](
        TaskSchedule(
          "order",
          1,
          "task",
          0,
          10,
          List[Physical](
            Physical(
              "1",
              "type")),
          List[Human](
            Human(
              "1",
              "name",
              List[Handles](
                Handles("type")))))))

    // Act
    val randomFileName = java.util.UUID.randomUUID.toString
    val filePath = System.getProperty("user.dir") + "/files/tests/" + randomFileName
    XMLWriter.writeToXml(filePath, schedule)

    // Assert
    val fileTemp = new File(filePath)
    val exists = fileTemp.exists
    fileTemp.delete()

    assert(exists == true)
  }
}