package isep.tap.ip.tests

import isep.tap.ip.model._
import org.scalatest.FunSuite
import scala.xml.XML
import isep.tap.ip.datareaders.XMLReader
import isep.tap.ip.services._

class SchedulerTests extends FunSuite {

  test("GenerateScheduleTest_Regular") {
    // Arrange
    val expected = Schedule(
      List[TaskSchedule](TaskSchedule("ORD_1", 1, "TSK_1", 0, 100,
        List[Physical](Physical("PRS_1", "PRST 1")),
        List[Human](Human("HRS_1", "Antonio", List[Handles](Handles("PRST 1"), Handles("PRST 2")))))))
    val filePath = "/tests/GenerateScheduleTest_Regular.xml"
    val production = XMLReader.getProductionDataFromFile(filePath)

    // Act
    val scheduler = ProductionFactory.newProductionFactory()
    val schedule = scheduler.getProductionSchedule(production, 1)

    // Assert
    assert(schedule == expected)
  }

  test("GenerateScheduleTest_InsufficientPhysicalResources") {
    // Arrange
    val expected = Schedule(List[TaskSchedule]())
    val filePath = "/tests/GenerateScheduleTest_InsufficientPhysicalResources.xml"
    val production = XMLReader.getProductionDataFromFile(filePath)

    // Act
    val scheduler = ProductionFactory.newProductionFactory()
    val schedule = scheduler.getProductionSchedule(production, 1)

    // Assert
    assert(schedule == expected)
  }

  test("GenerateScheduleTest_InsufficientHumanResources") {
    // Arrange
    val expected = Schedule(List[TaskSchedule]())
    val filePath = "/tests/GenerateScheduleTest_InsufficientHumanResources.xml"
    val production = XMLReader.getProductionDataFromFile(filePath)

    // Act
    val scheduler = ProductionFactory.newProductionFactory()
    val schedule = scheduler.getProductionSchedule(production, 1)

    // Assert
    assert(schedule == expected)
  }

  test("GenerateScheduleTest_Parallel") {
    // Arrange
    val expected = Schedule(List(
      TaskSchedule("ORD_1", 1, "TSK_1", 0, 10,
        List(Physical("PRS_1", "PRST 1")),
        List(Human("HRS_1", "Antonio",
          List(Handles("PRST 1"))))),
      TaskSchedule("ORD_1", 1, "TSK_2", 10, 25,
        List(Physical("PRS_3", "PRST 2")),
        List(Human("HRS_3", "Jose",
          List(Handles("PRST 2"))))),
      TaskSchedule("ORD_1", 2, "TSK_1", 10, 20,
        List(Physical("PRS_1", "PRST 1")),
        List(Human("HRS_1", "Antonio",
          List(Handles("PRST 1"))))),
      TaskSchedule("ORD_1", 2, "TSK_2", 25, 40,
        List(Physical("PRS_3", "PRST 2")),
        List(Human("HRS_3", "Jose",
          List(Handles("PRST 2")))))))

    val filePath = "/tests/GenerateScheduleTest_ParallelTest.xml"
    val production = XMLReader.getProductionDataFromFile(filePath)

    // Act
    val scheduler = ProductionFactory.newProductionFactory()
    val schedule = scheduler.getProductionSchedule(production, 2)

    // Assert
    assert(schedule == expected)
  }

  test("GenerateScheduleTest_Parallel_Milestone3_test1") {
    // Arrange
    val expected = Schedule(List(
      TaskSchedule("ORD_1", 1, "TSK_1", 0, 10,
        List(Physical("PRS_1", "PRST 1")),
        List(Human("HRS_1", "Antonio", List(Handles("PRST 1"))))),
      TaskSchedule("ORD_2", 1, "TSK_2", 0, 15,
        List(Physical("PRS_3", "PRST 2")),
        List(Human("HRS_2", "Maria", List(Handles("PRST 1"), Handles("PRST 2"))))),
      TaskSchedule("ORD_1", 2, "TSK_1", 15, 25,
        List(Physical("PRS_1", "PRST 1")),
        List(Human("HRS_1", "Antonio", List(Handles("PRST 1"))))),
      TaskSchedule("ORD_2", 2, "TSK_2", 15, 30,
        List(Physical("PRS_3", "PRST 2")),
        List(Human("HRS_2", "Maria", List(Handles("PRST 1"), Handles("PRST 2"))))),
      TaskSchedule("ORD_1", 1, "TSK_2", 30, 45,
        List(Physical("PRS_3", "PRST 2")),
        List(Human("HRS_2", "Maria", List(Handles("PRST 1"), Handles("PRST 2"))))),
      TaskSchedule("ORD_1", 2, "TSK_2", 45, 60,
        List(Physical("PRS_3", "PRST 2")), List(Human("HRS_2", "Maria", List(Handles("PRST 1"), Handles("PRST 2")))))))

    val filePath = "/tests/ip_2017_in_ok_2o_3p_2t_v1.xml"
    val production = XMLReader.getProductionDataFromFile(filePath)

    // Act
    val scheduler = ProductionFactory.newProductionFactory()
    val schedule = scheduler.getProductionSchedule(production, 3)

    // Assert
    assert(schedule == expected)
  }

  test("GenerateScheduleTest_Parallel_Milestone3_test2") {
    // Arrange
    val expected = Schedule(List(
      TaskSchedule("ORD_1", 1, "TSK_1", 0, 10,
        List(Physical("PRS_1", "PRST 1")),
        List(Human("HRS_1", "Antonio", List(Handles("PRST 1"))))),
      TaskSchedule("ORD_2", 1, "TSK_2", 0, 15,
        List(Physical("PRS_3", "PRST 2")),
        List(Human("HRS_2", "Maria", List(Handles("PRST 1"), Handles("PRST 2"))))),
      TaskSchedule("ORD_2", 2, "TSK_2", 0, 15,
        List(Physical("PRS_4", "PRST 2")),
        List(Human("HRS_3", "Jose", List(Handles("PRST 2"))))),
      TaskSchedule("ORD_1", 2, "TSK_1", 15, 25,
        List(Physical("PRS_1", "PRST 1")),
        List(Human("HRS_1", "Antonio", List(Handles("PRST 1"))))),
      TaskSchedule("ORD_1", 1, "TSK_2", 15, 30,
        List(Physical("PRS_3", "PRST 2")),
        List(Human("HRS_2", "Maria", List(Handles("PRST 1"), Handles("PRST 2"))))),
      TaskSchedule("ORD_1", 2, "TSK_2", 15, 30,
        List(Physical("PRS_4", "PRST 2")),
        List(Human("HRS_3", "Jose", List(Handles("PRST 2")))))))

    val filePath = "/tests/ip_2017_in_ok_2o_3p_2t_v2.xml"
    val production = XMLReader.getProductionDataFromFile(filePath)

    // Act
    val scheduler = ProductionFactory.newProductionFactory()
    val schedule = scheduler.getProductionSchedule(production, 3)

    // Assert
    assert(schedule == expected)
  }

  test("GenerateScheduleTest_Parallel_Milestone3_test3") {
    // Arrange
    val expected = Schedule(List(
      TaskSchedule("ORD_1", 1, "TSK_1", 0, 10,
        List(Physical("PRS_1", "PRST 1"), Physical("PRS_2", "PRST 1")),
        List(Human("HRS_1", "Antonio", List(Handles("PRST 1"))), Human("HRS_2", "Maria", List(Handles("PRST 1"), Handles("PRST 2"))))),
      TaskSchedule("ORD_2", 1, "TSK_2", 0, 15,
        List(Physical("PRS_3", "PRST 2")),
        List(Human("HRS_3", "Jose", List(Handles("PRST 2"))))),
      TaskSchedule("ORD_2", 2, "TSK_2", 0, 15,
        List(Physical("PRS_4", "PRST 2")),
        List(Human("HRS_4", "Manuela", List(Handles("PRST 2"))))),
      TaskSchedule("ORD_1", 2, "TSK_1", 15, 25,
        List(Physical("PRS_1", "PRST 1"), Physical("PRS_2", "PRST 1")),
        List(Human("HRS_1", "Antonio", List(Handles("PRST 1"))), Human("HRS_2", "Maria", List(Handles("PRST 1"), Handles("PRST 2"))))),
      TaskSchedule("ORD_1", 1, "TSK_2", 15, 30,
        List(Physical("PRS_3", "PRST 2")),
        List(Human("HRS_3", "Jose", List(Handles("PRST 2"))))),
      TaskSchedule("ORD_1", 2, "TSK_2", 15, 30,
        List(Physical("PRS_4", "PRST 2")),
        List(Human("HRS_4", "Manuela", List(Handles("PRST 2")))))))

    val filePath = "/tests/ip_2017_in_ok_2o_3p_2t_v3.xml"
    val production = XMLReader.getProductionDataFromFile(filePath)

    // Act
    val scheduler = ProductionFactory.newProductionFactory()
    val schedule = scheduler.getProductionSchedule(production, 3)

    // Assert
    assert(schedule == expected)
  }
}