package isep.tap.ip.tests

import isep.tap.ip.model._
import org.scalatest.FunSuite
import scala.xml.XML
import isep.tap.ip.datareaders.XMLReader

class XMLReaderTests extends FunSuite {

  test("ReadXML_ValidInput_FileRead") {

    // Arrange
    val expected = Production(
      List[Physical](Physical("PRS_1", "PRST 1")),
      List[Task](Task("TSK_1", 100, List[PhysicalResource](PhysicalResource("PRST 1")))),
      List[Human](Human("HRS_1", "Antonio", List[Handles](Handles("PRST 1"), Handles("PRST 2")))),
      List[Product](Product("PRD_1", "Product 1", List[Process](Process("TSK_1")))),
      List[Order](Order("ORD_1", "PRD_1", 1)))

    // Act
    val filePath = "/tests/GenerateScheduleTest_Regular.xml"
    val production = XMLReader.getProductionDataFromFile(filePath)

    // Assert
    assert(production == expected)
  }
}