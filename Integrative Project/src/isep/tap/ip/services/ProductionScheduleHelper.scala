package isep.tap.ip.services

import isep.tap.ip.model._
import isep.tap.ip.model.v2._

object ProductionScheduleHelper {

  /**
   * Verifies if there are enough Physical and Human Resources to process one Task
   */
  def verifyEnoughResourcesOneTask(task: Task, physicalResources: List[Physical], humans: List[Human]): Boolean =
    {
      /*
       * Create a map with the needed resources and the quantity needed
       * 
       * Map[PhysicalResource, Int]
       */
      val prTypeNeededForTask = task.resources.groupBy(tp => tp).map { case (k, v) => (k, v.size) }

      /*
       * Create a map with the available resources and the quantity available
       * 
       * Map[PhysicalResource, Int]
       */
      val prm = physicalResources.map(pr => pr.`type`).groupBy(t => t).map { case (k, v) => (k, v.size) }

      // Same for human resources
      val hrm = humans.flatMap(hr => hr.Handles.map(_.`type`)).groupBy(t => t).map { case (k, v) => (k, v.size) }

      //Verify enough resources
      prTypeNeededForTask.forall {
        case (k, v) => ((prm get k.prstype).getOrElse(0) >= v) &&
          ((hrm get k.prstype).getOrElse(0) >= v)
      }

    }

  /**
   * Verifies enough resources for the orders
   */
  def verifyEnoughResources(orders: List[Order],
                            products: List[Product],
                            tasks: List[Task],
                            physicalResources: List[Physical],
                            humans: List[Human]): Boolean = {

    // Get Tasks needed for the order                    
    val tasksNeededForOrder = orders.map(o => products.find(p => p.id.equals(o.prdref)).get.processes.map(_.tskref))
      .flatMap(t => t).toSet[String].toList
      .map(tID => tasks.find(t => t.id.equals(tID)).get)

    // Verify enough resources for each task
    tasksNeededForOrder.forall(task => verifyEnoughResourcesOneTask(task, physicalResources, humans))
  }

  def start(production: Production,
            processOrder: (Int, Product, List[Int], Order) => List[TaskSchedule]): Schedule =
    {

      // Processes the orders
      def processOrders(initialTime: Int,
                        orders: List[Order]): List[TaskSchedule] = orders match {

        case Nil => Nil
        case order :: tail =>
          val product = production.Products.find(p => p.id.equals(order.prdref)).get
          val prdRange = (1 to order.quantity).toList
          val tkscheduleList = processOrder(initialTime, product, prdRange, order)
          tkscheduleList ::: processOrders(tkscheduleList.last.end, tail)
      }
      val hasEnoughResources = verifyEnoughResources(production.Orders, production.Products, production.Tasks, production.PhysicalResources, production.HumanResources)

      hasEnoughResources match {
        case true =>
          val list = processOrders(0, production.Orders)
          val sucess = list.forall(tskSch => tskSch.physicalResources.size == tskSch.humanResources.size)
          sucess match {
            case true  => Schedule(list)
            case false => Schedule(Nil)
          }
        case false =>
          Schedule(Nil)
      }
    }

  private def convertOrdersToOrderTasks(orders: List[Order], production: Production): List[OrderTask] = orders match {
    case Nil                              => List[OrderTask]()
    case orderList if (orderList.isEmpty) => List[OrderTask]()
    case order :: tail =>
      convertToOrderTasks(production.Products.filter(p => p.id == order.prdref).head, order, production) match {
        case list if (list.isEmpty) => List[OrderTask]()
        case tkscheduleList         => tkscheduleList ::: convertOrdersToOrderTasks(tail, production)
      }
  }
  def convertToOrderTasks(product: Product,
                          order: Order,
                          production: Production): List[OrderTask] = {
    val tasksToProcess = production.Tasks.filter(x => product.processes.exists(p => p.tskref == x.id))
    getOrderTasks(1, order, tasksToProcess)
  }

  // Itera a quantidade
  private def getOrderTasks(productNumber: Int,
                            order: Order,
                            tasksToProcess: List[Task]): List[OrderTask] = productNumber match {
    case x if (order.quantity + 1 == x) => Nil
    case _ => {
      convertToOrderTask(tasksToProcess, productNumber, order, 1) match {
        case list if (list.isEmpty) => List[OrderTask]()
        case orderTasks             => orderTasks ::: getOrderTasks(productNumber + 1, order, tasksToProcess)
      }
    }
  }

  // itera as tasks do produto
  private def convertToOrderTask(tasks: List[Task],
                                 productNumber: Int,
                                 order: Order,
                                 taskSequence: Int): List[OrderTask] = tasks match {

    case Nil                            => List[OrderTask]()
    case taskList if (taskList.isEmpty) => List[OrderTask]()
    case o1 :: tail =>
      List(OrderTask(o1, taskSequence, order, productNumber)) ::: convertToOrderTask(tail, productNumber, order, taskSequence + 1)

  }

  def start(production: Production,
            processOrder: (Int, List[OrderTask]) => List[TaskSchedule]): Schedule =
    {

      // Processes the orders
      def processOrders(initialTime: Int,
                        orders: List[Order]): List[TaskSchedule] = orders match {

        case Nil => Nil
        case order :: tail =>
          val orderTasks = convertOrdersToOrderTasks(orders, production)
          processOrder(initialTime, orderTasks)
      }
      val hasEnoughResources = verifyEnoughResources(production.Orders, production.Products, production.Tasks, production.PhysicalResources, production.HumanResources)

      hasEnoughResources match {
        case true =>
          val list = processOrders(0, production.Orders)
          val sucess = list.forall(tskSch => tskSch.physicalResources.size == tskSch.humanResources.size)
          sucess match {
            case true  => Schedule(list)
            case false => Schedule(Nil)
          }
        case false =>
          Schedule(Nil)
      }
    }
}
