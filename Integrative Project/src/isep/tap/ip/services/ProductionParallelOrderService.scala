package isep.tap.ip.services

import isep.tap.ip.model._
import isep.tap.ip.model.v2._

object ProductionParallelOrderService extends Algorithm {

  /**
   * Processes one Task
   */
  def processTask(initialTime: Int,
                  tsk: Task,
                  productNumber: Int,
                  order: Order,
                  availablePR: List[Physical],
                  availableHR: List[Human]): Option[TaskSchedule] =
    {

      // List of physical resources grouped by type
      val prGroupedByType = availablePR.groupBy(pr => pr.`type`)

      // List of physical resources grouped by type
      val hrGroupedByType = availableHR
        .flatMap(hr => hr.Handles.map(_.`type`)).toSet[String]
        .map(prType => prType -> availableHR.filter(pr => pr.Handles.map(_.`type`).contains(prType)))
        .toMap[String, List[Human]]

      // Allocates Physical Resources
      val prl = allocatePhysicalResources(tsk.resources.map(_.prstype), prGroupedByType, Nil)

      // Allocates Human Resources to each physical
      val hrl = allocateHumanResources(tsk.resources.map(_.prstype), hrGroupedByType, Nil)

      // Validate sizes
      if (prl.size == hrl.size && hrl.size == tsk.resources.size)
        Option(TaskSchedule(order.id, productNumber, tsk.id, initialTime, initialTime + tsk.time, prl, hrl))
      else
        None
    }

  /**
   * Processes a the tasks in parallel
   */
  def parallelProcess(tasks: List[Task],
                      prdRange: List[Int],
                      initialTime: Int,
                      order: Order,
                      model: Production,
                      prAvailable: List[Physical],
                      hrAvailable: List[Human]): List[TaskSchedule] =

    (tasks, prdRange) match {
      case (Nil, Nil) => Nil
      case (task :: t1, productNumber :: t2) if (ProductionScheduleHelper.verifyEnoughResourcesOneTask(task, prAvailable, hrAvailable)) =>
        val opt = processTask(initialTime, task, productNumber, order, prAvailable, hrAvailable)

        opt match {
          case Some(tskSchedule) =>
            tskSchedule :: parallelProcess(t1, t2, initialTime, order, model, prAvailable.filter(pr => (!tskSchedule.physicalResources.contains(pr))), hrAvailable.filter(hr => (!tskSchedule.humanResources.map(f => f.id).contains(hr.id))))
          case None => parallelProcess(t1, t2, initialTime, order, model, prAvailable, hrAvailable)

        }
      case (tsk :: t1, prdNr :: t2) => parallelProcess(t1, t2, initialTime, order, model, prAvailable, hrAvailable)
    }

  /**
   * Creates a new Task Matrix
   */
  def createTaskMatrix(tskListEachProduct: List[List[Task]], prdRange: List[Int], tskSchCreated: List[TaskSchedule]): List[List[Task]] =
    (tskListEachProduct, prdRange) match {

      case (Nil, Nil) => Nil
      //Task already processed
      case (tskList :: t1, prdNr :: t2) if (tskSchCreated.exists(t => t.productNumber == prdNr)) => tskList.tail :: createTaskMatrix(t1, t2, tskSchCreated)
      case (tskList :: t1, prdNr :: t2) => tskList :: createTaskMatrix(t1, t2, tskSchCreated)
    }

  /**
   * Clears the matrix
   */
  def clearRangeNilList(tskListEachProduct: List[List[Task]], numberOfProducts: List[Int]): List[Int] =
    (tskListEachProduct, numberOfProducts) match {
      case (Nil, Nil)               => Nil
      //Task already processed
      case (Nil :: t1, prdNr :: t2) => clearRangeNilList(t1, t2)
      case (_ :: t1, prdNr :: t2)   => prdNr :: clearRangeNilList(t1, t2)
    }

  def processOrderBlocks(tskListEachProduct: List[List[Task]],
                         prdRange: List[Int],
                         initialTime: Int,
                         order: Order,
                         model: Production): List[TaskSchedule] =
    tskListEachProduct match {
      case Nil => Nil
      case _ =>
        val heads = tskListEachProduct.map(list => list.head)
        val lstTskSch = parallelProcess(heads, prdRange, initialTime, order, model, model.PhysicalResources, model.HumanResources)
        val newMatrix = createTaskMatrix(tskListEachProduct, prdRange, lstTskSch)
        val clearProcessedTaskList = newMatrix.filter(tL => tL != Nil)
        val newRange = clearRangeNilList(newMatrix, prdRange)
        val finalTime = lstTskSch.map(t => t.end).max
        lstTskSch ::: processOrderBlocks(clearProcessedTaskList, newRange, finalTime, order, model)
    }

  /**
   * Returns the available Humans
   */
  def availableHumans(prType: String,
                      humans: Map[String, List[Human]],
                      occupied: List[Human]): List[Human] =
    {
      humans.get(prType).get.filter(hr => (!occupied.contains(hr)))
    }

  /**
   * Allocates the Humans to the tasks to be processed
   */
  def allocateHumanResources(taskTypes: List[String],
                             hrGroupedByType: Map[String, List[Human]],
                             occupiedHR: List[Human]): List[Human] =

    taskTypes match {
      case Nil => Nil
      case t1 :: Nil =>
        val ahr = availableHumans(t1, hrGroupedByType, occupiedHR)
        ahr match {
          case Nil      => Nil
          case hr1 :: _ => hr1 :: Nil
        }
      case t1 :: tail =>
        val ahr = availableHumans(t1, hrGroupedByType, occupiedHR)
        ahr match {
          case Nil => Nil
          case availableHumanResources =>
            availableHumanResources.toStream.map(h => h :: allocateHumanResources(tail, hrGroupedByType, h :: occupiedHR))
              .find {
                case h :: Nil                          => false
                case h :: h1 if (h1.size == tail.size) => true
              }.getOrElse(Nil)
        }
    }

  /**
   * Retrieves the PhysicalResources available
   */
  def availablePhysicalResources(prType: String,
                                 physicalResources: Map[String, List[Physical]],
                                 occupied: List[Physical]): List[Physical] =
    {
      physicalResources.get(prType).get.filter(pr => (!occupied.contains(pr)))
    }

  /**
   * Allocates the physical resources to the task
   */
  def allocatePhysicalResources(taskTypes: List[String],
                                prGroupedByType: Map[String, List[Physical]],
                                occupiedPR: List[Physical]): List[Physical] =
    taskTypes match {
      case Nil => Nil
      case t1 :: tail =>
        val apr = availablePhysicalResources(t1, prGroupedByType, occupiedPR)
        val allocatedPR = apr.head
        allocatedPR :: allocatePhysicalResources(tail, prGroupedByType, allocatedPR :: occupiedPR)
    }

  /**
   * Processes the production using OrderOptimization
   */
  override def getProductionSchedule(production: Production, algorithm: Int): Schedule = {

    def processProduct(initialTime: Int, product: Product, prdRange: List[Int], order: Order): List[TaskSchedule] =
      {
        val t = prdRange.map(i => product).map(p => p.processes.map(_.tskref).map(tID => production.Tasks.find(t => t.id.equals(tID)).get))
        processOrderBlocks(t, prdRange, initialTime, order, production)
      }

    ProductionScheduleHelper.start(production, processProduct _)
  }
}