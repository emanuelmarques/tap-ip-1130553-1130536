package isep.tap.ip.services

import isep.tap.ip.model._
import isep.tap.ip.model.v2._

object ProductionSequencialSchedulingService extends Algorithm {

  override def getProductionSchedule(production: Production, algorithm: Int): Schedule = {
    Schedule(generateTaskScheduleList(production, production.Orders, None, 0))
  }

  private def generateTaskScheduleList(production: Production, orders: List[Order], currentSchedule: Option[List[TaskSchedule]], currentTime: Int): List[TaskSchedule] = {
    processOrders(currentTime, orders, production)
  }

  private def processOrders(initialTime: Int, orders: List[Order], production: Production): List[TaskSchedule] = orders match {
    case Nil                              => List[TaskSchedule]()
    case orderList if (orderList.isEmpty) => List[TaskSchedule]()
    case o1 :: tail =>
      processOrder(production.Products, production, o1, initialTime) match {
        case list if (list.isEmpty) => List[TaskSchedule]()
        case tkscheduleList         => tkscheduleList ::: processOrders(tkscheduleList.last.end, tail, production)
      }
  }

  private def processOrder(products: List[Product], production: Production, order: Order, time: Int): List[TaskSchedule] = {
    val optionProduct = products.find(p => p.id == order.prdref)
    optionProduct match {
      case None          => List[TaskSchedule]()
      case Some(product) => processProducts(product, production, 1, order, 0)
    }
  }

  private def processProducts(prod: Product, production: Production, current: Int, order: Order, time: Int): List[TaskSchedule] = current match {
    case x if (order.quantity + 1 == x) => Nil
    case _ => {
      processProduct(prod, production, current, order, time) match {
        case list if (list.isEmpty) => List[TaskSchedule]()
        case tkscheduleList         => tkscheduleList ::: processProducts(prod, production, current + 1, order, tkscheduleList.last.end)
      }
    }
  }

  private def processProduct(prod: Product, production: Production, productNumber: Int, order: Order, time: Int): List[TaskSchedule] = {
    val tasksToProcess = production.Tasks.filter(x => prod.processes.exists(p => p.tskref == x.id))
    processTasks(time, tasksToProcess, production, productNumber, order)
  }

  private def processTasks(time: Int, tasks: List[Task], production: Production, productNumber: Int, order: Order): List[TaskSchedule] = tasks match {
    case Nil => List[TaskSchedule]()
    case o1 :: tail =>
      processTask(order, o1, production, productNumber, time) match {
        case None     => List[TaskSchedule]()
        case Some(ts) => List(ts) ::: processTasks(time + o1.time, tail, production, productNumber, order)
      }
  }

  private def processTask(order: Order, task: Task, production: Production, productNumber: Int, time: Int): Option[TaskSchedule] = {
    val physicalsOptions = task.resources.map((r => production.PhysicalResources.find(p => p.`type` == r.prstype)))
    validatePhysicalOption(physicalsOptions) match {
      case list if (list.isEmpty) => None
      case physicals => humansForTask(List[Human](), physicals, production.HumanResources, 0, physicals.size) match {
        case list1 if (list1.isEmpty) => None
        case humans                   => Some(TaskSchedule(order.id, productNumber, task.id, time, time + task.time, physicals, humans))
      }
    }
  }

  private def humansForTask(finalList: List[Human],
                            physicals: List[Physical],
                            humans: List[Human],
                            counter: Int,
                            physicalsInitialSize: Int): List[Human] = counter match {
    case x if (x == physicalsInitialSize) => finalList
    case _ => {
      humanForPhysical(humans, physicals.head) match {
        case list if (list.isEmpty) => List[Human]()
        case humansForPhysical =>
          val humanX = humansForPhysical.head
          humansForTask(
            finalList ::: List(humanX),
            physicals.tail,
            humans.filter(h => h.id != humanX.id),
            counter + 1, physicalsInitialSize)
      }
    }
  }

  private def humanForPhysical(humans: List[Human], physical: Physical): List[Human] = {
    humans.filter(
      hu => hu.Handles.exists(
        ha => ha.`type` == physical.`type`))
  }

  private def validatePhysicalOption(physicalOptions: List[Option[Physical]]): List[Physical] = {
    physicalOptions.exists(p => p == None) match {
      case true  => List[Physical]()
      case false => physicalOptions.map(po => po.get)
    }
  }
}


  
  
  
  
  
  