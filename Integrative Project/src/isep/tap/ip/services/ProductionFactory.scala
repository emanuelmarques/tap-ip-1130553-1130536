package isep.tap.ip.services

import isep.tap.ip.model._

object ProductionFactory {
  private class AlgorithmImpl extends Algorithm {
    def getProductionSchedule(production: Production, algorithm: Int): Schedule = {
      val strategy = ProductionFactory.apply(production, algorithm)
      strategy match {
        case None => Schedule(List())
        case _    => strategy.get
      }
    }
  }

  def newProductionFactory(): Algorithm = new AlgorithmImpl

  private def apply(production: Production, algorithm: Int): Option[Schedule] =
    algorithm match {
      case 1 => Some(milestone1(production))
      case 2 => Some(milestone2(production))
      case 3 => Some(milestone3(production))
      case _ => None
    }

  private def milestone1(production: Production): Schedule = {
    ProductionSequencialSchedulingService.getProductionSchedule(production, 1)
  }

  private def milestone2(production: Production): Schedule = {
    ProductionParallelOrderService.getProductionSchedule(production, 2)
  }

  private def milestone3(production: Production): Schedule = {
    ProductionParallelService.getProductionSchedule(production, 3)
  }

}