package isep.tap.ip.services

import isep.tap.ip.model._
import isep.tap.ip.model.v2.OrderTask

object ProductionParallelService extends Algorithm {

  /**
   * Processes the production using OrderOptimization
   */
  override def getProductionSchedule(production: Production, algorithm: Int): Schedule = {

    def processOrders(initialTime: Int, tasks: List[OrderTask]): List[TaskSchedule] =
      {
        processBlocks(tasks, initialTime, production, List())
      }

    ProductionScheduleHelper.start(production, processOrders _)
  }

  def processBlocks(tskListEachProductEachOrder: List[OrderTask],
                    initialTime: Int,
                    production: Production, processedTasks: List[TaskSchedule]): List[TaskSchedule] =
    tskListEachProductEachOrder match {
      case Nil => Nil
      case _ =>
        val lstTskSch = parallelProcess(tskListEachProductEachOrder.sortBy(r => (r.taskSequence, r.productNumber, r.order.id)), initialTime, production, production.PhysicalResources, production.HumanResources)
        val newMatrix = filterProcessedTasks(tskListEachProductEachOrder, lstTskSch ::: processedTasks)
        val clearProcessedTaskList = newMatrix.filter(tL => tL != Nil)
        val finalTime = lstTskSch.map(t => t.end).max
        lstTskSch ::: processBlocks(clearProcessedTaskList, finalTime, production, lstTskSch ::: processedTasks)
    }

  /**
   * Processes a the tasks in parallel
   */
  def parallelProcess(tasks: List[OrderTask],
                      initialTime: Int,
                      production: Production,
                      prAvailable: List[Physical],
                      hrAvailable: List[Human]): List[TaskSchedule] =

    tasks match {
      case (Nil) => Nil
      case (task :: t1) if (ProductionScheduleHelper.verifyEnoughResourcesOneTask(task.task, prAvailable, hrAvailable) && verifySequence(tasks, task)) =>
        val opt = processTask(initialTime, task, prAvailable, hrAvailable)

        opt match {
          case Some(tskSchedule) =>
            val remainingTasks = tasks.filter(t => t != task)
            tskSchedule :: parallelProcess(remainingTasks.sortBy(r => (r.taskSequence, r.productNumber, r.order.id)),
              initialTime, production, prAvailable.filter(pr => (!tskSchedule.physicalResources.contains(pr))), hrAvailable.filter(hr => (!tskSchedule.humanResources.map(f => f.id).contains(hr.id))))
          case None => parallelProcess(t1.sortBy(r => (r.taskSequence, r.productNumber, r.order.id)), initialTime, production, prAvailable, hrAvailable)

        }
      case (tsk :: t1) => parallelProcess(t1.sortBy(r => (r.taskSequence, r.productNumber, r.order.id)), initialTime, production, prAvailable, hrAvailable)
    }

  def verifySequence(tasks: List[OrderTask], task: OrderTask): Boolean = {
    val productTasks = tasks.filter(t => t.order == task.order && t.productNumber == task.productNumber)
    productTasks.sortBy(r => (r.taskSequence, r.productNumber, r.order.id))
    task == productTasks.head
  }

  /**
   * Processes one Task
   */
  def processTask(initialTime: Int,
                  tsk: OrderTask,
                  availablePR: List[Physical],
                  availableHR: List[Human]): Option[TaskSchedule] =
    {

      // List of physical resources grouped by type
      val prGroupedByType = availablePR.groupBy(pr => pr.`type`)

      // List of physical resources grouped by type
      val hrGroupedByType = availableHR
        .flatMap(hr => hr.Handles.map(_.`type`)).toSet[String]
        .map(prType => prType -> availableHR.filter(pr => pr.Handles.map(_.`type`).contains(prType)))
        .toMap[String, List[Human]]

      // Allocates Physical Resources
      val prl = ProductionParallelOrderService.allocatePhysicalResources(tsk.task.resources.map(_.prstype), prGroupedByType, Nil)

      // Allocates Human Resources to each physical
      val hrl = ProductionParallelOrderService.allocateHumanResources(tsk.task.resources.map(_.prstype), hrGroupedByType, Nil)

      // Validate sizes
      if (prl.size == hrl.size && hrl.size == tsk.task.resources.size)
        Option(TaskSchedule(tsk.order.id, tsk.productNumber, tsk.task.id, initialTime, initialTime + tsk.task.time, prl, hrl))
      else
        None
    }

  /**
   * filter the already processed tasks
   */
  def filterProcessedTasks(tskListEachProduct: List[OrderTask], tskSchCreated: List[TaskSchedule]): List[OrderTask] =
    {
      tskListEachProduct.filter(x => !isAlreadyProcessed(x, tskSchCreated))
    }

  def isAlreadyProcessed(task: OrderTask, processed: List[TaskSchedule]): Boolean = {

    processed match {
      case Nil => false
      case _   => processed.exists(p => p.productNumber == task.productNumber && p.order == task.order.id && p.task == task.task.id)
    }
  }
}