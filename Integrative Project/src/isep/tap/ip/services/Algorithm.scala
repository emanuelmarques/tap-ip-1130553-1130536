package isep.tap.ip.services

import isep.tap.ip.model._

trait Algorithm {
  def getProductionSchedule(production: Production, algorithm: Int): Schedule 
}