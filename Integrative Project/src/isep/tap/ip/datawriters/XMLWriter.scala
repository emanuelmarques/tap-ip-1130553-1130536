package isep.tap.ip.datawriters

import scala.xml.XML
import isep.tap.ip.model._

object XMLWriter {

  def writeToXml(filename: String, schedule: Schedule) {
    val output =
      <Schedule xmlns="http://www.dei.isep.ipp.pt/ip_2017" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/ip_2017 ip_2017_out.xsd">
        {
          schedule.taskScheduleList.map(tsl =>
            <TaskSchedule order={ tsl.order } productNumber={ tsl.productNumber.toString() } task={ tsl.task } start={ tsl.start.toString() } end={ tsl.end.toString() }>
              <PhysicalResources>
                { tsl.physicalResources.map(pr => <Physical id={ pr.id }/>) }
              </PhysicalResources>
              <HumanResources>
                { tsl.humanResources.map(hr => <Human name={ hr.name }/>) }
              </HumanResources>
            </TaskSchedule>)
        }
      </Schedule>

    XML.save(filename, output, "UTF-8", true, null)
  }
}